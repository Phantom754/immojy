import I18nProvider from "./app/provider/I18nProvider"
import  {FormattedMessage} from 'react-intl'

function App() {
  return (
    <>
      <I18nProvider>
        <FormattedMessage id="APP.TITLE" />
      </I18nProvider>
    </>
  )
}

export default App
