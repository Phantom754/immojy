import { FC, PropsWithChildren, useMemo, useState } from "react"
import { IntlProvider } from "react-intl"
import frMessage from '../../shared/translate/messages/fr.json'
import enMessage from '../../shared/translate/messages/en.json'

interface Messages {
  [key: string]: string | any;
}

const allMessages: Messages = {
    fr: frMessage,
    en: enMessage
}

const I18nProvider: FC<PropsWithChildren> = ({children}) => {
  const [locale] = useState<string>('en');
  const messages = useMemo(() => allMessages[locale],[locale])  
  console.log("messages",messages);
  
  return (
    <IntlProvider messages={messages} locale={locale} defaultLocale={locale}>
      {children}
    </IntlProvider>
  )
}

export default I18nProvider