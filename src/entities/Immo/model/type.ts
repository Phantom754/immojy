type Accordion  = {
    icon: JSX.Element,
    heading: string,
    detail: string
}