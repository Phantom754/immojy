export type Immo = {
    name: string,
    price: string,
    detail: string,
    image: string 
}